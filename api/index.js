const express = require('express');
const app = express();
const cors = require('cors');
// parser
const bodyParser = require('body-parser');
//parser les requêtes
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
// routes
require('./routes/routes.js')(app);
//mongoose
const mongoose = require('mongoose');
const options = {
	autoIndex: false, // Don't build indexes
	reconnectTries: 30, // Retry up to 30 times
	reconnectInterval: 500, // Reconnect every 500ms
	poolSize: 10, // Maintain up to 10 socket connections
	// If not connected, return errors immediately rather than waiting for reconnect
	bufferMaxEntries: 0
};
const mongooseUrl =
	process.env.NODE_ENV === 'dev'
		? 'mongodb://localhost:27017'
		: 'mongodb://mongo_db:27017';

console.log('mongooseURL ', mongooseUrl);

const connectWithRetry = () => {
	console.log('MongoDB connection with retry');
	mongoose
		.connect(mongooseUrl, options)
		.then(() => {
			console.log('\n\nMongoDB is connected \n\n');
		})
		.catch(err => {
			console.log(err);
			console.log('MongoDB connection unsuccessful, retry after 5 seconds.');
			setTimeout(connectWithRetry, 5000);
		});
};

connectWithRetry();

// port mapping
const port = process.env.NODE_ENV === 'dev' ? 3333 : 3000;

app.listen(port, function() {
	console.log('App listening on port ' + port);
});
