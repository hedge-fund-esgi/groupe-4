const Message = require('../models/messageModel.js');
const User = require('../models/userModel');

exports.Message_new = async function(req, res){
    try{
        console.log(req.body);
        if(req.body.sender_id && req.body.target_id && req.body.tx_ids){
            const sender_id = req.body.sender_id;
            const target_id = req.body.target_d;
            const tx_ids = req.body.tx_ids;

            User.find({ _id: sender_id }, (err, response) => {
                if(response.length > 0){
                    console.log("sender finded in db : "+response);

                    User.find({ id: target_id }, (err, response) => {
                        if(response.length > 0){
                            console.log("target finded on db : "+response);

                            let new_message = new Message();
                            new_message.sender_id = sender_id;
                            new_message.target_id = target_id;
                            new_message.tx_ids = tx_ids;
                            new_message.save(function(err, message) {
                                if (err) res.status(400).send(err);
                                res.status(201).json(new_message);
                            });
                        }else{
                            console.log("err : can't find target on db");
                            res.sendStatus(400);
                        }
                    });
                }else{
                    console.log("err : can't find sender in db");
                    res.sendStatus(400);
                }
            });
        }else{
            console.log("Not enough args defined in req.");
            res.sendStatus(400);
        }
    }
    catch(error){
        console.log('error Message_new', error);
		res.status(500).send('Internal server error');
    }
};

exports.Message_fetch = async function(req, res){
    try{
        console.log(req.body);
        if(req.query.id){
            const user_id = req.query.id;
            Message.find({ target_id: user_id }, (err, response) => {
                if (response.length > 0) {
					res.status(200).json(response);
				} else {
					res.sendStatus(404);
				}
            })
        }else{
            console.log("Not enough args defined in req.");
            res.sendStatus(400);
        }
    }
    catch(error){
        console.log('error Message_fetch', error);
        res.status(500).send('Internal server error');
    }
};