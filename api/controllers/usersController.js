const jwt = require('jsonwebtoken');
const User = require('../models/userModel');
const config = require('../config/secrets');
const crypto = require('crypto');
const regMail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

/*
name
password
email
*/
exports.User_register = async function(req, res) {
	try {
		console.log(req.body.name);
		const userMail = req.body.email.toLowerCase();
		// test request content
		if (regMail.test(String(userMail)) && req.body.name && req.body.password) {
			// find existing user
			User.find({ email: userMail }, (err, response) => {
				if (response.length > 0) {
					res.status(200).send({ res: 'exist' });
				}
				// user not exist => create
				else {
					const passwordHash = crypto
						.createHash('sha256')
						.update(req.body.password)
						.digest('hex');

					console.log('inscription hash', passwordHash);
					let new_user = new User();
					new_user.name = req.body.name.toLowerCase();
					new_user.email = userMail;
					new_user.password = passwordHash;
					new_user.save(function(err, user) {
						if (err) res.status(400).send(err);
						res.status(201).json(user);
					});
				}
			});
		} else {
			res.sendStatus(400);
		}
	} catch (error) {
		console.log('error User_registrer', error);
		res.status(500).send('Internal server error');
	}
};
exports.User_login = function(req, res) {
	try {
		console.log('route login ', req.body);
		const userMail = req.body.email.toLowerCase();
		if (req.body.email && req.body.password) {
			User.findOne({ email: userMail }, function(err, user) {
				if (err || !user) {
					console.log('no user fund');
					res.status(400).send(err);
				} else if (user.email === userMail) {
					const hash = crypto
						.createHash('sha256')
						.update(req.body.password)
						.digest('hex');
					console.log('password match', user.password === hash);
					if (user.password === hash) {
						console.log('hash');
						jwt.sign(
							{ user: user },
							config.secrets.jwt_key,
							{
								expiresIn: '30 days'
							},
							(err, token) => {
								if (err) res.send(err);
								res.status(200).json({ token });
							}
						);
					} else {
						res.status(400).send('Wrong email or password');
					}
				} else {
					res.status(500).send('Database not available');
				}
			});
		} else res.status(400).send({ err: 'Bad request' });
	} catch (error) {
		console.log('error User_login', error);
		res.status(500).send('Internal server error');
	}
};
exports.User_modifier = async function(req, res, next) {
	try {
		console.log('req put ', req.body);
		const userMail = req.body.email.toLowerCase();
		if (regMail.test(String(userMail)) && req.body.username && req.body.id) {
			User.find({ _id: req.body.id }, (err, response) => {
				if (response.length > 0) {
					User.updateOne(
						{ _id: req.body.id },
						{
							name: req.body.username,
							email: userMail
						},
						function(err, affected) {
							if (affected.ok) {
								res.sendStatus(200);
							} else {
								res.sendStatus(304);
							}
						}
					);
				} else {
					res.sendStatus(404);
				}
			});
		} else {
			res.sendStatus(400);
		}
	} catch (error) {
		console.log(error);
		res.sendStatus(500);
	}
};
exports.User_infos = async function(req, res, next) {
	try {
		console.log('req put ', req.body);
		if (req.body.id) {
			User.findOne({ _id: req.body.id }, (err, response) => {
				if (response.length > 0) {
					res.status(200).json(response);
				} else {
					res.sendStatus(404);
				}
			});
		} else {
			res.sendStatus(400);
		}
	} catch (error) {
		console.log(error);
		res.sendStatus(500);
	}
};
exports.User_delete = async function(req, res, next) {
	try {
		console.log(req.body);
		if (req.body.id) {
			User.deleteOne({ _id: req.body.id }, (err, response) => {
				console.log(response);
				if (response.deletedCount > 0) {
					res.sendStatus(200);
				} else {
					res.sendStatus(404);
				}
			});
		}
	} catch (error) {
		console.log(error);
		res.sendStatus(500);
	}
};
