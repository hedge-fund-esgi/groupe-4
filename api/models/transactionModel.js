var mongoose = require('mongoose');

/*
 * automatic mongo _id
 * type : type de transaction (achat ou vente)
 * owner_id : identifiant de l'utilisateur ayant effectué la transaction
 * dateCrea : date actuelle lors de la création de la transaction
 * target : cible de la transaction (smart-contract ou un des fonds)
 * from_addr : source de la transaction (bank_account ou SC_Token)
 * fiat : montant de monnaie mis en jeu pour achat/vente de token
 * token : montant de token mis en jeu pour achat/vente de token ou achat/vente de shares
 * share : montant de shares mis en jeu pour achat/vente de shares
*/

var TransactionSchema = new mongoose.Schema({
	tx_id: Number,
	tx_type: String,
	owner_id: String,
	dateCrea: { type: Date, default: Date.now },
	target: String,
	from_addr: String,
	fiat: Number,
	token: Number,
	share: Number
});

module.exports = mongoose.model('Transaction', TransactionSchema);
