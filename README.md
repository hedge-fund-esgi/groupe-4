# hedge-fund project
Pour lancer le projet il faut un service mongoDB qui tourne sur le port 27017
Ouvrir un terminal et rendez vous dans le dossier API/ pour y faire : npm run dev
Ouvrir un terminal et rendez vous dans le dossier FRONT/ pour y faire : npm run dev
Rendez-vous sur localhost:8080
Ouvrez la collection sur POSTMAN et cliquez sur init_fund pour charger les données d'un FONDS

Vous pourrez ensuite depuis l'interface vous enregistrer et vous connecter.
Vous pourrez également acheter des TOKENS en EURO puis échanger les TOKEN contre des SHARE.
Il y à des graphiques qui affiche votre répartition dans celle du fonds et des autres investisseurs.
Il y à d'autres graphique des statistiques et un historique des transactions.

L'architecture docker-compose est fonctionnelle mais les requêtes API n'y fonctionne pas :
- MongoDB
- API (NodeJS)
- Front (VueJS)

Vuetify - Composant graphique 
Chartjs - Graph
Axios - Requêtage type Postman

Nombre d’issue : 15 
Page mon compte :
- 1 Je veux un espace personel

DashBoard (dynamique):
-3 Je veux suivre la valeur de mon investissement
- 10 Je veux des statistiques sur mes performances financières.
- 9 Je veux pouvoir partager mon dashboard avec un tiers
- 12 Je veux connaitre ma repartition en share dans le fond.
- 13 Je veux connaitre la composition de fond. 
- 7 Je veux pouvoir consulter mon historique de transactions
- 15 Je veux des alertes

Achat vente :
- 5 Je veux pouvoir acheter / vendre des shares 
- 6 Je veux pouvoir convertir mes tokens en euros / euro en tokens

(Optionnelles) 
Tutoriel:
- 8 Je veux un tutoriel pour utiliser l'application
Site responsive :
- 14 Je veux pouvoir accéder à mon site depuis un mobile

