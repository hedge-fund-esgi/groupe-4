import Register from './register'

describe('@views/register', () => {
  it('is a valid view', () => {
    expect(Register).toBeAViewComponent()
  })
})
