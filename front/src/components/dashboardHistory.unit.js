import DashboardHistory from './dashboardHistory'

describe('@components/dashboardHistory', () => {
  it('exports a valid component', () => {
    expect(DashboardHistory).toBeAComponent()
  })
})
