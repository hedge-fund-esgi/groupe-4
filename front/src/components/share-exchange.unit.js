import ShareExchange from './share-exchange'

describe('@components/share-exchange', () => {
  it('exports a valid component', () => {
    expect(ShareExchange).toBeAComponent()
  })
})
